---
tiers:
  top:
    - loki prime:
        role:
        - CC
        reason: Invisibility is very strong, very fast, invaluable CC on ult. Irradiating Disarm has best scaling in game.
    - trinity prime:
        role:
        - Utility (Heal)
        - Utility (Energy)
        reason: Great healer and infinite energy returns, amazing on a team. Lacks AoE DPS
    - frost prime:
        role:
        - CC
        - Damage
        reason: Insane defense with new snow globe, able to CC everything 3 ways to monday.
    - inaros:
        role:
        - CC
        - Tank
        reason: Self reviving tank with ability to CC, and heal allies. Only (minor) downside is JOAT modding.
    - nova prime:
        role:
        - CC
        - Utility (Movement)
        reason: Best team movement ability, very strong CC on ult, very high scaling and defense on antimatter absorb, very fast movement.
    - excalibur:
        role:
        - CC
        - Damage
        reason: Very strong frame with good scaling on ult and survivability, able to CC and take out priority targets quickly with blind finisher. Ult damage does not fall off.
    - valkyr:
        role:
        - Tank
        reason: True invincibility, poor AOE DPS, good as team revive medic. Invulnerable to all damage and status changes during ult.
    - cold chroma:
        role:
        - CC
        - Tank
        reason: Great synergy from double armor buff on 2 and 3 with arcane guardian and Armored Agility/Steel Fiber. Nice cold proc to mitigate damage.
    - rhino prime:
        role:
        - CC
        - Tank
        reason: Very tanky, strong endgame defensive scaling, awesome CC. Changes to Charge make Rhino more powerful.
    - saryn prime:
        role:
        - Damage
        reason: Prime adds significantly more energy and armor. Great debuffs, single target, and AOE damage. High skill barrier. Suffers from JOAT modding
    - wukong:
        role:
        - Tank
        reason: Literally refuses to die; ability to drop aggro; high range ult. Using Rage + duration = true invulnerablity. Allows use of weapons during invuln but higher energy cost than Valkyr
    - mirage:
        role:
        - CC
        - Damage
        reason: Absolutely amazing weapon DPS scaling, good defense with blind, good for many strats.
    - ash prime:
        role:
        - Damage
        reason: Great damage with ult, with invulnerability during cast
    - ivara:
        role:
        - Damage
        - Utility
        reason: Fantastic team synergy using Quiver, ult allows for great AOE damage. Utility includes group invisibility and ziplines
    - nekros:
        role:
        - Utility (Drop)
        - Utility (Revive)
        reason: Synergises well with Carrier/Prime Vacuum and Equilibrium. Teammate resurection and amazing energy/health management.
  very-good:
    - heat chroma:
        role:
        - Tank
        reason: Health stacks with %health regen from grace to great effect.
    - banshee:
        role:
        - CC
        - Utility (Damage)
        reason: Very high CC and area of control as well as the strongest buff in game. Allows for super fast kills of enemies.
    - electric chroma:
        role:
        - CC
        - Damage
        reason: Shields and high damage electric shocks as well as AOE stuns are a great boon, but armor from cold is preferable.
    - atlas:
        role:
        - Tank
        - Utility (Walls)
        reason: Extremely tanky with 2nd highest armor stat and knockdown immunity, has unique but niche abilities. Spamable ability and drops a taunting rumbler with augment
    - toxin chroma:
        role:
        - Damage
        - Tank
        reason: Offensively oriented with good buffs to reload speed, but worst at Chroma's strongest suit- defense. Use with bullet hoses or shotguns with long reload times.
    - nezha:
        role:
        - Damage
        - Utility
        reason: Extremely squishy frame with great mobility and utility abilities. Ultimate ability is good CC, but lacks damage and range.
    - mesa:
        role:
        - Damage
        reason: Easy to build into a long-term area defense drone which allows for longer defenses. Ridiculous single target damage.
    - nyx prime:
        role:
        - CC
        reason: Infinite scaling on defense missions with absorb, good CC. Many skills are inferior to other frames.
    - vauban:
        role:
        - CC
        reason: Good defense scaling, point farming, and hands-free whole map coverage with bastille.
    - equinox:
        role:
        - Utility (Heal)
        reason: Mend's heal can full heal a whole team in endgame with one enemy killed due to scaling off max enemy health. Maim's damage is good as well. Has mitigation too.
  good:
    - zephyr:
        role:
        - Tank
        reason: Very tanky, very mobile. Is immune to melee enemies since she's flying all the time in parkour 2.0.
    - ember prime:
        role:
        - Damage
        reason: Abilities are very hard to take advantage of but can work. Good against infested or for speed runs of exterminate missions.
    - volt prime:
        role:
        - CC
        - Tank
        reason: Super tanky, good shield/crit synergy, excellent for CC and shields on ult, great for exterm/cap/sabotage missions.
    - mag prime:
        role:
        - CC
        - Damage
        reason: Amazing Corpus killer, good for cheese strats and farming, can solo defense missions easily with explosives by pulling ammo.
    - hydroid:
        role:
        - CC
        - Utility
        reason: Very strong team utility, healing, and CC, alright for farming and survivals.
  decent:
    - limbo:
        role:
        - Utility (Statis)
        reason: Invincibility at cost of DPS, good resource management, good for some niche strategies, very awkward to use.
    - oberon:
        role:
        - Utility (Heal)
        reason: Good generalist frame, damage is outclassed by other frames, healing is outclassed by other frames. Decent for solo, okay on a team.
---
Frames are rated on their scaling into very high endgame content for survivability and ease of use.  
Some of these frames are very strong and even vital for certain niche strategies.  
This is rated on how well each one would perform in a randomized team of 4 players.
