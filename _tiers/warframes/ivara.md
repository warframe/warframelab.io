---
display_name: "Ivara"
health: [75, 225]
shield: [100, 300]
power: [175, 262.5]
armor: 65
speed: 1.15
conclave: 0
---
With her quiver of tactical arrows, this huntress prowls unseen and strikes without warning.
