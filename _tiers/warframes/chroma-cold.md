---
display_name: "Chroma (Cold)"
frame: "chroma"
role:
- "CC"
- "Tank"
score: 90
---
Great synergy from double armor buff on 2 and 3 with arcane guardian and Armored Agility/Steel Fiber. Nice cold proc to mitigate damage.
