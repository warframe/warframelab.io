---
display_name: "Valkyr"
health: [100, 300]
shield: [50, 150]
power: [100, 150]
armor: 600
speed: 1.1
conclave: 90
---
Forged in the labs of the Zanuka project, the original Valkyr was subject to cruel experiments, leaving her scarred, angry and frighteningly adept at killing.
