---
display_name: "Zephyr"
health: [150, 450]
shield: [150, 450]
power: [100, 150]
armor: 15
speed: 1.15
conclave: 60
---
Specializing in air attacks and mobility, Zephyr dominates from above.
