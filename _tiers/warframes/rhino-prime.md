---
display_name: "Rhino Prime"
health: [100, 300]
shield: [150, 450]
power: [100, 150]
armor: 275
speed: 1.0
conclave: 150
base: "rhino"
variant: "prime"
vaulted: 1
---
Red lights flashing on stark, white walls. Davis is running ahead of me, dropping his notes. We're running for our lives. The fear gives me a strange perspective - I'm out of my body. I've forgotten how I got here. I don't recognize this place.

Davis and I slam pinned against a cell door and he shouts at me. I give him a dumb look. I can't hear him, the sirens, anything, only the muffled throb of terror in my head. I turn away from Davis down the hall and I see it. The hulking mass, flickering red, glinting like steel and fresh blood. Its skin changes, flowing like mercury when I'm blinded by the sudden muzzle-flashes. They do no good. The beast surges forward and the security men become crimson mist and gore.

I'm a statue, a cornered animal. A gate opens inside me and recognition floods in. I have seen this monster before. I have cut its shell and eviscerated its brothers. I have given it pain and measured its response. I have crafted then rejected countless like it. But I've never seen this beast so close, without the shield, without restraints. I have never seen it... free.

I know I will die so I just watch with curious acceptance. The beast squats down, shovelling a heap of gore into its mouth. It is watching me with vague eyes, a sense of recognition, ancestral memory. It knows who I am and what I've done. It rears up like a bear and roars, shattering the lights and casting us into darkness. I can hear it lumbering toward me, its metal fingers rending the walls, but I know I am dead. I close my eyes and stand ready to pay.

I feel the pull on my arm and realize Davis got the cell open. He tugs me into the cell beyond and I fall on my back. I see Davis standing at the open door, waiting, as the monster tears towards us.

Suddenly I could live through this I shout, "Davis, close the goddamn door!" - But he shakes his head eyes wide as moons. He shouts, "Watch!" over the roaring and rending of metal.

Then silence. Davis is panting, laughing? The beast fills the doorway, inches from him, dripping in blood, but still without violence. It stands there, looking at its hands. Davis whispers, "No one would have believed me."

I crawl up the wall to stand, opposite the door. I've never seen this cell, a cold place with an array of shelves. A morgue? "Where are we, Davis?"

"This is where they keep them. The ones from Zariman." I'm thrown, what was the Zariman? The ship that never returned? "Davis, what's going on?"

Davis turns to me, a smile forming - "What's going on is..." he turns back to the beast now silent and calm.
"...big, fat promotions."
