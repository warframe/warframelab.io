---
display_name: "Oberon"
health: [125, 375]
shield: [100, 300]
power: [100, 150]
armor: 150
speed: 1.0
conclave: 90
---
Equally adept at healing friends or striking down the enemy, Oberon embodies the balance Tenno are sworn to uphold.
