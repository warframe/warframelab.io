---
display_name: "Inaros"
health: [550, 2200]
shield: [0, 0]
power: [100, 150]
armor: 200
speed: 1.0
conclave: 0
---
Risen from the sands, Inaros commands the desert's fearsome power.
