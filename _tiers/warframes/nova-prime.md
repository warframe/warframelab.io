---
display_name: "Nova Prime"
health: [100, 300]
shield: [100, 300]
power: [175, 262.5]
armor: 65
speed: 1.2
conclave: 80
base: "nova"
variant: "prime"
vaulted: 0
---
Nova Prime wreaks devastation on her enemies using volatile anti-matter. Featuring altered mod polarities for greater customization.
