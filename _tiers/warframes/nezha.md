---
display_name: "Nezha"
health: [75, 225]
shield: [75, 225]
power: [150, 225]
armor: 175
speed: 1.15
conclave: 60
---
A petite and playful facade conceals this frame's immense power.
