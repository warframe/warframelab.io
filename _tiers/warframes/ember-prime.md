---
display_name: "Ember Prime"
frame: "ember-prime"
role:
- "Damage"
score: 50
---
Abilities are very hard to take advantage of but can work. Good against infested or for speed runs of exterminate missions.
