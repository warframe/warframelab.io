---
display_name: "Nyx Prime"
health: [100, 300]
shield: [125, 375]
power: [150, 225]
armor: 50
speed: 1.125
conclave: 0
base: "nyx"
variant: "prime"
vaulted: 0
---
Infiltrate the minds of your enemies with Nyx Prime. Featuring altered mod polarities for greater customization.
