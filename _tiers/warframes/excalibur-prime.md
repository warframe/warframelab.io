---
display_name: "Excalibur Prime"
frame: "excalibur-prime"
role:
- "CC"
- "Damage"
score: 90
---
Very strong frame with good scaling on ult and survivability, able to CC and take out priority targets quickly with blind finisher. Ult damage does not fall off.
