---
display_name: "Chroma (heat)"
frame: "chroma"
role:
- "Tank"
score: 70
---
Health stacks with %health regen from grace to great effect.
