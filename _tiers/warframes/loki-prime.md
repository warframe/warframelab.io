---
display_name: "Loki Prime"
health: [75, 225]
shield: [75, 225]
power: [175, 262.5]
armor: 65
speed: 1.25
conclave: 50
base: "loki"
variant: "prime"
vaulted: 0
---
Confuse, deceive and destroy with Loki Prime. Featuring altered mod polarities for greater customization.
