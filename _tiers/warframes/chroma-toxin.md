---
display_name: "Chroma (toxin)"
frame: "chroma"
role:
- "Damage"
- "Tank"
score: 50
---
Offensively oriented with good buffs to reload speed, but worst at Chroma's strongest suit- defense. Use with bullet hoses or shotguns with long reload times.
