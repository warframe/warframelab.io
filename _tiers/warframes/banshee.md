---
display_name: "Banshee"
frame: "banshee"
role:
- "CC"
- "Utility (Damage)"
score: 75
---
Very high CC and area of control as well as the strongest buff in game. Allows for super fast kills of enemies.
