---
display_name: "Chroma (Electric)"
frame: "chroma"
role:
- "CC"
- "Damage"
score: 70
---
Shields and high damage electric shocks as well as AOE stuns are a great boon, but armor from cold is preferable.
