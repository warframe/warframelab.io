---
display_name: "Equinox"
frame: "equinox"
role:
- "Utility (Heal)"
score: 70
---
Mend's heal can full heal a whole team in endgame with one enemy killed due to scaling off max enemy health. Maim's damage is good as well. Has mitigation too.
