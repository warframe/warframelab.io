---
display_name: "Trinity Prime"
health: [100, 300]
shield: [150, 450]
power: [150, 225]
armor: 15
speed: 1.1
conclave: 70
base: "trinity"
variant: "prime"
vaulted: 0
---
Become the bastion that defends allies using powerful healing abilities with Trinity Prime. Featuring altered mod polarities for greater customization.
