---
display_name: "Volt Prime"
health: [100, 300]
shield: [150, 450]
power: [200, 300]
armor: 100
speed: 1.0
conclave: 80
base: "volt"
variant: "prime"
vaulted: 0
---
A glorious warrior from the past, Volt Prime features the same abilities as Volt, but with unique mod polarities for greater customization.
