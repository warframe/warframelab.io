---
display_name: "Ember"
frame: "ember"
role:
- "Damage"
score: 45
---
Abilities are very hard to take advantage of but can work. Good against infested or for speed runs of exterminate missions.
