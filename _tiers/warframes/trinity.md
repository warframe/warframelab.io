---
display_name: "Trinity"
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 15
speed: 1.0
conclave: 70
---
Trinity is great for players who prefer a supportive role. Warframes with healing technology are rare making Trinity a great equalizer when the odds are stacked against the Tenno.
