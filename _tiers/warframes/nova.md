---
display_name: "Nova"
health: [100, 300]
shield: [75, 225]
power: [150, 225]
armor: 65
speed: 1.2
conclave: 80
---
Nova uses electromagnetic energy to contain and control highly volatile antimatter that fuels her abilities.
