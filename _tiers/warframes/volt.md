---
display_name: "Volt"
health: [100, 300]
shield: [150, 450]
power: [100, 150]
armor: 15
speed: 1.0
conclave: 80
---
Volt can create and harness electrical elements. This is a high-damage warframe perfect for players looking for a potent alternative to gunplay.
