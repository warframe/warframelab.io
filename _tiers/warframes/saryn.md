---
display_name: "Saryn"
health: [125, 375]
shield: [100, 300]
power: [150, 225]
armor: 175
speed: 0.95
conclave: 150
---
Saryn's venomous attacks are horrifyingly effective against organic and synthetic enemies, and her ability to "shed" her skin makes her very elusive.
