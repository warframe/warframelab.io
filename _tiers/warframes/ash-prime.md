---
display_name: "Ash Prime"
frame: "ash-prime"
role:
- "Damage"
score: 90
---
Great damage with ult, with invulnerability during cast
