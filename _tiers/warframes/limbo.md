---
display_name: "Limbo"
health: [100, 300]
shield: [75, 225]
power: [150, 225]
armor: 65
speed: 1.15
conclave: 80
---
Limbo manipulates the very planes of existence to divide his enemies and conquer them in the rift.
