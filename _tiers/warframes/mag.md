---
display_name: "Mag"
health: [75, 225]
shield: [150, 450]
power: [100, 150]
armor: 65
speed: 1.0
conclave: 80
---
With full command of surrounding magnetic energy, Mag is an expert at enemy manipulation.
