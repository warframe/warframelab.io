---
display_name: "Atlas"
frame: "atlas"
role:
- "Tank"
- "Utility (Walls)"
score: 70
---
Extremely tanky with 2nd highest armor stat and knockdown immunity, has unique but niche abilities. Spamable ability and drops a taunting rumbler with augment
