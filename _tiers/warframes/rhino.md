---
display_name: "Rhino"
health: [100, 300]
shield: [150, 450]
power: [100, 150]
armor: 190
speed: 0.95
conclave: 146
---
Rhino is the heaviest Warframe, combining offensive and defensive capabilities.
