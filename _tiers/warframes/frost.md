---
display_name: "Frost"
health: [100, 300]
shield: [150, 450]
power: [100, 150]
armor: 300
speed: 0.95
conclave: 90
---
By channeling moisture and vapor in the surrounding environment, Frost creates formidable defenses and lethal attacks from sub zero conditions.
