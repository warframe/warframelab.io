it uses [jekyll](https://atom.io/packages/jekyll) for site generation, so look at that documentation if you want to know more.

---

## creating new content

most content just uses markdown, although you'd be best off consulting the jekyll documentation if you want to fully know everything there is that you can do.

#### Pages

[pages](https://jekyllrb.com/docs/pages/) are simply named html files. `index.html` lives at the root of the repository, and it's the homepage. Aside from that there's a few other pages, and it's pretty easy to make more.

* `index.html` - the homepage

#### misc file explanation

* `_config.yml` - the configuration file for jekyll
* `.gitlab-ci.yml` - instructions that tell gitlab on how the repository should be built
* `.gitignore` - anything listed in here will never ever be committed to the repository
* `Gemfile` - A list of ruby gems to install when the `bundle install` command is run

#### directory explanation

* `_layouts/` - files that explain the actual structure of the HTML that will be displayed
* `_includes/` - small reusable HTML snippets to be included into layouts
* `_assets/` - all of the static content, like images, videos, fonts, or CSS
* `_data/` - hard data about anything and everything, like who the members are, the bust sizes of the various heroines, or how much dank weed the protagonist smokes
* `_i18n/` - localization files, in case your website wants to support more than 1 language


---

## how to test it locally

This section isn't totally necessary, but it could be helpful to know if you want to see how your changes will look before you commit them to the repository.

#### dependencies to install:

* [ruby](https://www.ruby-lang.org) - the language that jekyll is built in
* [bundler](http://bundler.io/) - allows you to easily install the dependencies
* [nginx](http://nginx.org/en/download.html) - allows you to run a local webserver

#### steps:

1. clone repository to wherever you want
1. open up a command prompt
1. enter repository's directory and run `bundle install`
1. use `jekyll build --watch`, which will generate the website into `_site/`
1. modify the nginx config to point to the `_site/` directory
1. start up nginx, then nagivate to `localhost` in your web browser
    * the site should automatically regenerate whenever you make changes, so just refresh your browser
