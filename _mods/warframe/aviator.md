---
polarity: "vazarin"
cost: 4
max_rank: 3
conclave: [1,1,2,3]
effect: ["-5%","-10%","-15%","-20%"]
---
Reduced damage while airborne by {{page.effect[0]}}
