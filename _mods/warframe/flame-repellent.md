---
polarity: "vazarin"
cost: 4
max_rank: 5
conclave: [3,3,6,9,12,15]
effect: ["10%","20%","30%","40%","50%","60%"]
---
{{page.effect[0]}} Heat Resistance
