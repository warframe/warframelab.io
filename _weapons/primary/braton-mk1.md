---
display_name: "MK1-Braton"
slot: "primary"
type: "assault rifle"
trigger_type: "auto"
base: "braton"
variant: "mk1"
damage:
  impact: 4.5
  puncture: 4.5
  slash: 9.0
mastery: 0
crit_chance: 8.0
crit_mult: 1.5
status_chance: 5.0
reload: 2.0
max_ammo: 540
magazine: 60
accuracy: 40.0
fire_rate: 7.5
noise_level: 45.0
flight_speed: 0
conclave: 10
released: "vanilla"
---
The MK1-Braton is the standard issue rifle for all Tenno. A versatile weapon that can be customized to support a wide variety of play styles.
