---
display_name: "Braton Vandal"
slot: "primary"
type: "assault rifle"
trigger_type: "auto"
base: "braton"
variant: "vandal"
damage:
  impact: 12.3
  puncture: 1.8
  slash: 21.0
mastery: 0
crit_chance: 10.0
crit_mult: 2.0
status_chance: 10.0
reload: 1.8
max_ammo: 350
magazine: 50
accuracy: 33.3
fire_rate: 7.5
noise_level: 45.0
flight_speed: 0
conclave: 0
released: "6.3"
---
The Braton's high rate of fire and accuracy make it a favorite among Tenno.
