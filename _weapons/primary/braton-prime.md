---
display_name: "Braton Prime"
slot: "primary"
type: "assault rifle"
trigger_type: "auto"
base: "braton"
variant: "prime"
damage:
  impact: 1.75
  puncture: 12.25
  slash: 21.0
mastery: 0
crit_chance: 10.0
crit_mult: 2.0
status_chance: 20.0
reload: 2.2
max_ammo: 375
magazine: 75
accuracy: 28.6
fire_rate: 9.6
noise_level: 45.0
flight_speed: 0
conclave: 30
released: "9.0"
---
A classic Orokin weapon, Braton Prime features modified damage levels and a larger magazine over the standard model.
