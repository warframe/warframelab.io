---
display_name: "Braton"
slot: "primary"
type: "assault rifle"
trigger_type: "auto"
damage:
  impact: 6.6
  puncture: 6.6
  slash: 6.8
mastery: 0
crit_chance: 10.0
crit_mult: 1.5
status_chance: 5.0
reload: 2.0
max_ammo: 540
magazine: 45
accuracy: 28.6
fire_rate: 8.8
noise_level: 45.0
flight_speed: 0
conclave: 30
released: "vanilla"
---
The Braton's high rate of fire and accuracy make it a favorite among Tenno.
