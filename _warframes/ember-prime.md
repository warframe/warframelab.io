---
health: [100, 300]
shield: [125, 375]
power: [150, 225]
armor: 125
speed: 1.1
conclave: 120
base: "ember"
variant: "prime"
vaulted: 1
---
Three figures waited behind a simple table. Their attention on a single chair, bathed in light. An old woman's voice from the shadow: 'Send her in'. Across the room a security officer, stern and plain, opened the door. The outline of a young woman appeared at the door. She hesitated, but only for an instant, then crossed the room and sat.

There was a gasp as the light hit her face. Her right eye was bright and blinking, but her left was a greasy slit. Her skin had been burned moon-white. Her mouth was a sagging gash without lips or expression. Her military beret was pulled snug over a scarred and hairless scalp.

The old voice: 'Your name is Kaleen.' Kaleen nodded. 'You were the principal investigator of the Zariman?' Kaleen's voice was a jagged whisper, a rigid face. 'Yes.'

Kaleen coughed, straightened: 'The Zariman was lost making the fold from Saturn to the Outer gates. Mechanical failure. I notified families and filled a report with the inspectors. Nothing ever returns from the fold, so I closed the case.'

'But you reopened the case, days later.'

'I didn't believe it myself until I stepped aboard the ship. It was completely intact, full environmental, as if it had never left.'

'And the crew was gone.'

'Not exactly.' Kaleen hesitated. 'We thought it was empty but we began to find...' Her face twitched at remembered pain, 'We began to find children hiding in the ship.'

'And that is when you violated procedure?'

Kaleen bowed her head, a tear welling in her sightless eye. 'They were children. They were afraid. They needed comfort.'

'So you broke quarantine and this happened to you.'

There was silence as Kaleen touched her face, 'So what have you done with them?'

The old woman gestured for the officer to take Kaleen away. The meeting was over. When Kaleen reached the door she twisted out of his grip and shot back, 'Why would you do that? Why did you put children on military ship?'
'We didn't. That would violate procedure.'
