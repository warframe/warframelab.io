---
health: [100, 300]
shield: [175, 525]
power: [100, 150]
armor: 300
speed: 0.95
conclave: 90
base: "frost"
variant: "prime"
vaulted: 1
---
Frost Prime has the same chilling abilities as Frost but its altered mod polarities offer different possibilities for customization. It is also rumored that this Warframe has a unique reaction to certain Orokin technology.
