---
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 15
speed: 1.1
conclave: 60
---
Using sonic attacks and acoustic target detection, Banshee is well suited for stealth gameplay and is capable of filling both attack, and support roles.
