---
health: [100, 300]
shield: [100, 300]
power: [100, 150]
armor: 250
speed: 1.0
conclave: 0
base: "excalibur"
variant: "prime"
exclusive: 1
---
The Sentients had won. They had turned our weapons, our technology, against us. The more advanced we became, the greater our losses. The war was over unless we found a new way. In our desperation we turned to the Void. The blinding night, the hellspace where our science and reason failed.

We took the twisted few that had returned from that place. We built a frame around them, a conduit of their affliction. Gave them the weapons of the old ways. Gun and blade. A new warrior, a new code was born. These rejects, these Tenno, became our saviors. Warrior-Gods cast in steel and fury striking our enemies in a way they could never comprehend. Excalibur was the first.
- Orokin 'Warframe' Archives
