---
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 15
speed: 1.1
conclave: 50
---
Mind control and psychic attacks make Nyx a very dangerous foe. Her ability to reach into enemy consciousness and manipulate their behavior can turn the tide of the battle.
