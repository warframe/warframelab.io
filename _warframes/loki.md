---
health: [75, 225]
shield: [75, 225]
power: [150, 225]
armor: 65
speed: 1.25
conclave: 50
---
Desired by advanced players, Loki offers a variety of specialized reconfiguring abilities. The creativity of Loki's powers allows players to master the battlefield through manipulation.
