---
health: [125, 375]
shield: [100, 300]
power: [200, 300]
armor: 225
speed: 1.0
conclave: 70
base: "saryn"
variant: "prime"
vaulted: 0
---
A golden blossom conceals deadly nectar. Featuring altered mod polarities for greater customization.
