---
health: [100, 300]
shield: [150, 450]
power: [100, 150]
armor: 65
speed: 1.0
conclave: 80
base: "mag"
variant: "prime"
vaulted: 1
---
Warframe Archive - Debrief excerpt:

We sat strapped in, safeties off, waiting for the punch. Waiting for death. Through my filthy porthole I saw stars among the outlines of the other Splinter ships queuing for the Solar Rail. It would soon grip us with an incomprehensible power and cast us through the void into the mouth of our enemy.

I watched the ships one-by-one bending and gone. Each crammed with zero-tech soldiers sucking stale air, white knuckling their percussion rifles. Each filled with a desperation that comes from extinction. Our ship would be the last to cross the gap. Our ship had special cargo.

It was essentially empty. Just ten men, like me, strapped in with the best zero-tech suits and weapons the empire could build... and "it". "It" stood in the aisle, a slender and eyeless metal form. A Tenno inside its Warframe. Vaguely human, vaguely feminine. Was this armor or some ornate carapace for the monster that lived inside? I strained against the harness as the ship yawed for final approach. I could see the Tenno standing there freely. Solemn and gold-gleaming, oblivious to the inertial force.

I had been, until then, a Tenno denier. They were ghosts, propaganda, twisted casualties of the void era. Not possibly real. Yet here it was in the flesh. The Empire, in their desperation, was going to turn the demons loose and hope for the best. Who did we fear more, the enemy or this monster? We had our safeties off, could we trust it? Then it didn't matter anymore. The punch came - and our windows became blinding. When we could see again our ship was somewhere else, shattered and dead in an instant.

My lungs were flattened, eyes full of death. Ship debris glittered like a night snow. The alien blue star was dark and blinding beyond us. The countless articulating worm-ships of our enemy, ringed in glowing discs, undulating and heat-bursting the surviving soldiers like me. This is where I died. I was in R-disc, sweeping over my right and setting my blood on fire. My vision flattened, the hearing muffled and buzzed. I could feel the side of my face going slack and wet.

I was in a dying dream. I saw a bright spot blurring and weaving toward me. I felt a tug toward it from the metal clasps on my suit. It reached me, rising up - a gleaming beast, a plume of golden wings rising and unfolding behind it. An angel. It snatched me from my death. I could feel my lungs fill as it wrapped me in its wings. Its Void Shield shimmered blue, strained under the enemy beams. I felt a suddenly tug of acceleration. I closed my eyes and held on it like a child.
I awoke on the floor, the sting of crisping flesh on my face and side. It was standing over me, the wings gone. I heard the cracking of weapons echoing down the corridor. Maybe the mission would be saved, but I was dying and so I waved my hand to send it away. I felt a pistol thrust in my hand as I was heaved to my feet. The angel had saved me, pulled me from hell, but it would not pity me. I was to die on my feet, by its side. I turned my good side toward the gunfire and raised the gun. It nodded, its outstretched metal hand surging and pulsing in ancient shapes as blue shimmered around me. It turned, drawing its blade and together we surged headlong into the hailstorm of death and fire that awaited us.
