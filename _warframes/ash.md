---
health: [150, 450]
shield: [100, 300]
power: [100, 150]
armor: 65
speed: 1.15
conclave: 90
---
Ash is great for players looking for a more stealthy approach to combat. Lethal abilities are complemented by powers of distraction.
