---
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 350
speed: 1.0
conclave: 50
variant: "cold"
base: "chroma"
---
A master of the deadly elements, Chroma can alter his damage output by changing his energy color. Cold damage type with: white, grey, and some faded colors.
