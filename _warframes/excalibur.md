---
health: [100, 300]
shield: [100, 300]
power: [100, 150]
armor: 225
speed: 1.0
conclave: 70
---
A perfect balance of mobility and offense, Excalibur is the ideal Warframe for new players.
