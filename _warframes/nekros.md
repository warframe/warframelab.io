---
health: [100, 300]
shield: [90, 270]
power: [100, 150]
armor: 65
speed: 1.1
conclave: 70
---
Nekros uses his dark powers to manipulate his enemies, both living and dead.
