---
health: [150, 450]
shield: [125, 375]
power: [100, 150]
armor: 150
speed: 1.20
conclave: 90
base: "ash"
variant: "prime"
---
Distraction and subterfuge become lethal weapons with Ash Prime. Featuring altered mod polarities for greater customization.
