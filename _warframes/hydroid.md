---
health: [100, 300]
shield: [115, 345]
power: [100, 150]
armor: 65
speed: 1.05
conclave: 80
---
Rising from the ocean depths, Hydroid harnesses the power of water to a devastating effect.
