---
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 450
speed: 0.9
conclave: 40
---
Titan of stone, lord of the earthly elementals.
