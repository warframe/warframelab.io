---
health: [125, 375]
shield: [75, 225]
power: [100, 150]
armor: 65
speed: 1.1
conclave: 50
---
With a steady hand and quick reflexes, Mesa is a true gunfighter.
