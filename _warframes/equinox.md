---
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 100
speed: 1.15
conclave: 60
---
Split between day and night, Equinox manifests aggressive and defensive forms at will.
