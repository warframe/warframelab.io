---
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 100
speed: 1.1
conclave: 120
---
Ember is a nightmare for light-armoured targets. Ember can super-heat the air which opens up surprising crowd-control possibilities.
