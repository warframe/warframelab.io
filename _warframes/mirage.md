---
health: [80, 240]
shield: [80, 240]
power: [150, 225]
armor: 65
speed: 1.2
conclave: 0
---
A master of illusion, Mirage confounds the enemy in a spectacle of style and power.
