---
health: [100, 300]
shield: [125, 375]
power: [100, 150]
armor: 225
speed: 0.95
conclave: 10
---
A primal warrior with the heart of a trickster.
