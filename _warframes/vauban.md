---
health: [100, 300]
shield: [75, 225]
power: [150, 225]
armor: 50
speed: 1.0
conclave: 80
---
The highly tactical Vauban uses his powers to create deadly traps that can zap, imprison and dimensionally crush enemies.
