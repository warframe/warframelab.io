---
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 350
speed: 1.0
conclave: 50
variant: "electricity"
base: "chroma"
---
A master of the deadly elements, Chroma can alter his damage output by changing his energy color. Electric damage type with: blue, purple, and black hues.
