---
health: [100, 300]
shield: [100, 300]
power: [150, 225]
armor: 350
speed: 1.0
conclave: 50
variant: "heat"
base: "chroma"
---
A master of the deadly elements, Chroma can alter his damage output by changing his energy color. Heat damage type with: red, magenta, brown, orange and bright yellow hues.
